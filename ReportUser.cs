﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace EvidentaPractica
{
    public partial class ReportUser : Telerik.WinControls.UI.RadForm
    {
        int idUser;
        public ReportUser(int idStudent)
        {
            InitializeComponent();
            idUser = idStudent;
        }

        private void ReportUser_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ReportsDataSet.taskuri' table. You can move, or remove it, as needed.
            this.taskuriTableAdapter.Fill(this.ReportsDataSet.taskuri,idUser);

            this.reportUserViewer.RefreshReport();
        }
    }
}
