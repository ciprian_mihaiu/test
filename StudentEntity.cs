﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidentaPractica
{
    class StudentEntity
    {
        string nume;
        string cnp;
        string adresa;
        string institutie;
        int an;
        int nota;

        public string Nume
        {
            get { return this.nume; }
            set { this.nume = value; }
        }

        public string Cnp
        {
            get { return this.cnp; }
            set { this.cnp = value; }
        }

        public string Adresa
        {
            get { return this.adresa; }
            set { this.adresa = value; }
        }

        public string Institutie
        {
            get { return this.institutie; }
            set { this.institutie = value; }
        }

        public int An
        {
            get { return this.an; }
            set { this.an = value; }
        }

        public int Nota
        {
            get { return this.nota; }
            set { this.nota = value; }
        }
    }
}
