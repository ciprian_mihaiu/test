﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidentaPractica
{
    class TaskEntity
    {
        string denumire;
        string dataCreare;
        string timpLucrat;
        string descriere;
        int status;

        public int Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        public string TimpLucrat
        {
            get { return this.timpLucrat; }
            set { this.timpLucrat = value; }
        }

        public string Denumire
        {
            get { return this.denumire; }
            set { this.denumire = value; }
        }

        public string DataCreare
        {
            get { return this.dataCreare; }
            set { this.dataCreare = value; }
        }

        public string Descriere
        {
            get { return this.descriere; }
            set { this.descriere = value; }
        }
    }
}
