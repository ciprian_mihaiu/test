﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidentaPractica
{
    static class GestioneazaBD
    {
        private static string GetConnectionString()
        {
            return @"Data Source=CIPRIANPC\SQLEXPRESS;Initial Catalog=AplicatiePractica;Integrated Security=False;User ID=ciprian;Password=sqlpass123;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
        }

        public static SqlConnection CreareConexiune()
        {
            SqlConnection vCon = new SqlConnection(GetConnectionString());
            vCon.Open();
            return vCon;
        }

        public static void InchideConexiune(SqlConnection pCon)
        {
            pCon.Close();
        }

        internal static string GetTimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
    }
}
