﻿namespace EvidentaPractica
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginFormPanel = new Telerik.WinControls.UI.RadPanel();
            this.loginFormGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loginButton = new Telerik.WinControls.UI.RadButton();
            this.cancelButton = new Telerik.WinControls.UI.RadButton();
            this.numeUtilizatorLabel = new Telerik.WinControls.UI.RadLabel();
            this.parolaUtilizatorTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.parolaUtilizatorLabel = new Telerik.WinControls.UI.RadLabel();
            this.numeUtilizatorDropDown = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.loginFormPanel)).BeginInit();
            this.loginFormPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loginFormGroupBox)).BeginInit();
            this.loginFormGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loginButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeUtilizatorLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaUtilizatorTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaUtilizatorLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeUtilizatorDropDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // loginFormPanel
            // 
            this.loginFormPanel.Controls.Add(this.loginFormGroupBox);
            this.loginFormPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginFormPanel.Location = new System.Drawing.Point(0, 0);
            this.loginFormPanel.Name = "loginFormPanel";
            this.loginFormPanel.Size = new System.Drawing.Size(360, 260);
            this.loginFormPanel.TabIndex = 0;
            // 
            // loginFormGroupBox
            // 
            this.loginFormGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.loginFormGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loginFormGroupBox.Controls.Add(this.loginButton);
            this.loginFormGroupBox.Controls.Add(this.cancelButton);
            this.loginFormGroupBox.Controls.Add(this.numeUtilizatorLabel);
            this.loginFormGroupBox.Controls.Add(this.parolaUtilizatorTextBox);
            this.loginFormGroupBox.Controls.Add(this.parolaUtilizatorLabel);
            this.loginFormGroupBox.Controls.Add(this.numeUtilizatorDropDown);
            this.loginFormGroupBox.HeaderText = "Login";
            this.loginFormGroupBox.Location = new System.Drawing.Point(57, 33);
            this.loginFormGroupBox.Name = "loginFormGroupBox";
            this.loginFormGroupBox.Size = new System.Drawing.Size(249, 188);
            this.loginFormGroupBox.TabIndex = 4;
            this.loginFormGroupBox.Text = "Login";
            // 
            // loginButton
            // 
            this.loginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.loginButton.Location = new System.Drawing.Point(133, 145);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(88, 24);
            this.loginButton.TabIndex = 5;
            this.loginButton.Text = "Login";
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(25, 145);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(88, 24);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // numeUtilizatorLabel
            // 
            this.numeUtilizatorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numeUtilizatorLabel.Location = new System.Drawing.Point(25, 30);
            this.numeUtilizatorLabel.Name = "numeUtilizatorLabel";
            this.numeUtilizatorLabel.Size = new System.Drawing.Size(37, 18);
            this.numeUtilizatorLabel.TabIndex = 2;
            this.numeUtilizatorLabel.Text = "Nume";
            // 
            // parolaUtilizatorTextBox
            // 
            this.parolaUtilizatorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.parolaUtilizatorTextBox.Location = new System.Drawing.Point(25, 104);
            this.parolaUtilizatorTextBox.Name = "parolaUtilizatorTextBox";
            this.parolaUtilizatorTextBox.PasswordChar = '●';
            this.parolaUtilizatorTextBox.Size = new System.Drawing.Size(196, 20);
            this.parolaUtilizatorTextBox.TabIndex = 1;
            this.parolaUtilizatorTextBox.UseSystemPasswordChar = true;
            // 
            // parolaUtilizatorLabel
            // 
            this.parolaUtilizatorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.parolaUtilizatorLabel.Location = new System.Drawing.Point(25, 80);
            this.parolaUtilizatorLabel.Name = "parolaUtilizatorLabel";
            this.parolaUtilizatorLabel.Size = new System.Drawing.Size(37, 18);
            this.parolaUtilizatorLabel.TabIndex = 3;
            this.parolaUtilizatorLabel.Text = "Parola";
            // 
            // numeUtilizatorDropDown
            // 
            this.numeUtilizatorDropDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numeUtilizatorDropDown.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.numeUtilizatorDropDown.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InCubic;
            this.numeUtilizatorDropDown.Location = new System.Drawing.Point(25, 54);
            this.numeUtilizatorDropDown.Name = "numeUtilizatorDropDown";
            this.numeUtilizatorDropDown.Size = new System.Drawing.Size(196, 20);
            this.numeUtilizatorDropDown.TabIndex = 0;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.loginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(360, 260);
            this.Controls.Add(this.loginFormPanel);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(560, 360);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(368, 290);
            this.Name = "LoginForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(560, 360);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.loginFormPanel)).EndInit();
            this.loginFormPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loginFormGroupBox)).EndInit();
            this.loginFormGroupBox.ResumeLayout(false);
            this.loginFormGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loginButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeUtilizatorLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaUtilizatorTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaUtilizatorLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeUtilizatorDropDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel loginFormPanel;
        private Telerik.WinControls.UI.RadLabel parolaUtilizatorLabel;
        private Telerik.WinControls.UI.RadLabel numeUtilizatorLabel;
        private Telerik.WinControls.UI.RadTextBox parolaUtilizatorTextBox;
        private Telerik.WinControls.UI.RadDropDownList numeUtilizatorDropDown;
        private Telerik.WinControls.UI.RadGroupBox loginFormGroupBox;
        private Telerik.WinControls.UI.RadButton loginButton;
        private Telerik.WinControls.UI.RadButton cancelButton;
    }
}