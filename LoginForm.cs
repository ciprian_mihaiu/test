﻿using EvidentaPractica.Servicii;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EvidentaPractica
{
    public partial class LoginForm : Telerik.WinControls.UI.RadForm
    {
        IndrumatorForm formaIndrumator;
        StudentForm formaStudent;
        public LoginForm()
        {
            InitializeComponent();
            numeUtilizatorDropDown.DataSource = UserServices.GetNumeUtilizatori();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (numeUtilizatorDropDown.SelectedValue == null || parolaUtilizatorTextBox.Text == string.Empty) { return; }
            string parolaUtilizator = UserServices.CripteazaParola(parolaUtilizatorTextBox.Text);
            if (UserServices.UtilizatorValid(numeUtilizatorDropDown.SelectedValue.ToString(), parolaUtilizator))
            {
                if (UserServices.TipUtilizator(numeUtilizatorDropDown.SelectedValue.ToString()) == Constante.TipuriUtilizator.indrumator)
                {
                    DeschideFormaIndrumator();
                }
                else
                {
                    DeschideFormaStudent();
                }
            }
            else
            {
                MessageBox.Show("Wrong Login Information!");
                ResetLoginForm();
            }
        }

        private void DeschideFormaStudent()
        {
            formaStudent = new StudentForm(numeUtilizatorDropDown.SelectedValue.ToString());
            this.Hide();
            formaStudent.ShowDialog();
        }

        private void DeschideFormaIndrumator()
        {
            formaIndrumator = new IndrumatorForm();
            this.Hide();
            formaIndrumator.ShowDialog();
        }

        private void ResetLoginForm()
        {
            parolaUtilizatorTextBox.Text = string.Empty;
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
