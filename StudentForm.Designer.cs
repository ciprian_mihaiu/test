﻿namespace EvidentaPractica
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.Docking.AutoHideGroup autoHideGroup1 = new Telerik.WinControls.UI.Docking.AutoHideGroup();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.showMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.taskuriMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.adaugaTaskMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.controaleMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.numeLabel = new Telerik.WinControls.UI.RadLabel();
            this.logoutButton = new Telerik.WinControls.UI.RadButton();
            this.mainDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.taskuriWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.taskuriGridView = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.documentTabStrip1 = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.toolTabStrip2 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.adaugaTaskWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.adaugaTaskGroupBox = new System.Windows.Forms.GroupBox();
            this.descriereTaskTextBox = new Telerik.WinControls.UI.RadTextBoxControl();
            this.adaugaTaskButton = new Telerik.WinControls.UI.RadButton();
            this.cancelTaskButton = new Telerik.WinControls.UI.RadButton();
            this.descriereTaskLabel = new Telerik.WinControls.UI.RadLabel();
            this.denumireTaskTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.denumireTaskLabel = new Telerik.WinControls.UI.RadLabel();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.controaleWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.reportButton = new Telerik.WinControls.UI.RadButton();
            this.terminaTaskButton = new Telerik.WinControls.UI.RadButton();
            this.dockWindowPlaceholder1 = new Telerik.WinControls.UI.Docking.DockWindowPlaceholder();
            this.startTimerButton = new Telerik.WinControls.UI.RadButton();
            this.waitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.stopTimerButton = new Telerik.WinControls.UI.RadButton();
            this.parolaPanel = new Telerik.WinControls.UI.RadPanel();
            this.parolaLabel = new Telerik.WinControls.UI.RadLabel();
            this.parolaTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.parolaButton = new Telerik.WinControls.UI.RadButton();
            this.reseteazaParolaMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDock)).BeginInit();
            this.mainDock.SuspendLayout();
            this.taskuriWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            this.documentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).BeginInit();
            this.documentTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).BeginInit();
            this.toolTabStrip2.SuspendLayout();
            this.adaugaTaskWindow.SuspendLayout();
            this.adaugaTaskGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaTaskButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelTaskButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            this.controaleWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminaTaskButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startTimerButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopTimerButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaPanel)).BeginInit();
            this.parolaPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parolaLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.parolaPanel);
            this.radGroupBox1.Controls.Add(this.radMenu1);
            this.radGroupBox1.Controls.Add(this.numeLabel);
            this.radGroupBox1.Controls.Add(this.logoutButton);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Informatii";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(732, 43);
            this.radGroupBox1.TabIndex = 2;
            this.radGroupBox1.Text = "Informatii";
            // 
            // radMenu1
            // 
            this.radMenu1.Dock = System.Windows.Forms.DockStyle.None;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.showMenuItem});
            this.radMenu1.Location = new System.Drawing.Point(5, 16);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(143, 20);
            this.radMenu1.TabIndex = 2;
            this.radMenu1.Text = "radMenu1";
            // 
            // showMenuItem
            // 
            this.showMenuItem.AccessibleDescription = "Show";
            this.showMenuItem.AccessibleName = "Show";
            this.showMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuSeparatorItem1,
            this.taskuriMenuItem,
            this.radMenuSeparatorItem2,
            this.adaugaTaskMenuItem,
            this.radMenuSeparatorItem3,
            this.controaleMenuItem,
            this.radMenuSeparatorItem4,
            this.reseteazaParolaMenuItem,
            this.radMenuSeparatorItem5});
            this.showMenuItem.Name = "showMenuItem";
            this.showMenuItem.Text = "Deschide";
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // taskuriMenuItem
            // 
            this.taskuriMenuItem.AccessibleDescription = "Taskuri";
            this.taskuriMenuItem.AccessibleName = "Taskuri";
            this.taskuriMenuItem.Name = "taskuriMenuItem";
            this.taskuriMenuItem.Text = "Taskuri";
            this.taskuriMenuItem.Click += new System.EventHandler(this.taskuriMenuItem_Click);
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adaugaTaskMenuItem
            // 
            this.adaugaTaskMenuItem.AccessibleDescription = "Adauga Task";
            this.adaugaTaskMenuItem.AccessibleName = "Adauga Task";
            this.adaugaTaskMenuItem.Name = "adaugaTaskMenuItem";
            this.adaugaTaskMenuItem.Text = "Adauga Task";
            this.adaugaTaskMenuItem.Click += new System.EventHandler(this.adaugaTaskMenuItem_Click);
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // controaleMenuItem
            // 
            this.controaleMenuItem.AccessibleDescription = "Controale";
            this.controaleMenuItem.AccessibleName = "Controale";
            this.controaleMenuItem.Name = "controaleMenuItem";
            this.controaleMenuItem.Text = "Controale";
            this.controaleMenuItem.Click += new System.EventHandler(this.controaleMenuItem_Click);
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numeLabel
            // 
            this.numeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numeLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.numeLabel.Location = new System.Drawing.Point(463, 16);
            this.numeLabel.Name = "numeLabel";
            this.numeLabel.Size = new System.Drawing.Size(54, 18);
            this.numeLabel.TabIndex = 1;
            this.numeLabel.Text = "Logat ca ";
            // 
            // logoutButton
            // 
            this.logoutButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.logoutButton.Location = new System.Drawing.Point(617, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(110, 26);
            this.logoutButton.TabIndex = 0;
            this.logoutButton.Text = "Logout";
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // mainDock
            // 
            this.mainDock.ActiveWindow = this.controaleWindow;
            this.mainDock.CausesValidation = false;
            this.mainDock.Controls.Add(this.radSplitContainer1);
            this.mainDock.Controls.Add(this.toolTabStrip3);
            this.mainDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDock.IsCleanUpTarget = true;
            this.mainDock.Location = new System.Drawing.Point(0, 43);
            this.mainDock.MainDocumentContainer = this.documentContainer1;
            this.mainDock.Name = "mainDock";
            this.mainDock.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.mainDock.RootElement.MinSize = new System.Drawing.Size(0, 0);
            autoHideGroup1.Windows.Add(this.dockWindowPlaceholder1);
            this.mainDock.SerializableAutoHideContainer.RightAutoHideGroups.Add(autoHideGroup1);
            this.mainDock.Size = new System.Drawing.Size(732, 412);
            this.mainDock.TabIndex = 3;
            this.mainDock.TabStop = false;
            this.mainDock.Text = "radDock1";
            // 
            // taskuriWindow
            // 
            this.taskuriWindow.Controls.Add(this.taskuriGridView);
            this.taskuriWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.taskuriWindow.Location = new System.Drawing.Point(6, 29);
            this.taskuriWindow.Name = "taskuriWindow";
            this.taskuriWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.taskuriWindow.Size = new System.Drawing.Size(506, 300);
            this.taskuriWindow.Text = "Taskuri";
            // 
            // taskuriGridView
            // 
            this.taskuriGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskuriGridView.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.taskuriGridView.MasterTemplate.AllowAddNewRow = false;
            this.taskuriGridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.taskuriGridView.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.taskuriGridView.Name = "taskuriGridView";
            this.taskuriGridView.Size = new System.Drawing.Size(506, 300);
            this.taskuriGridView.TabIndex = 0;
            this.taskuriGridView.Text = "taskuriGridView";
            this.taskuriGridView.UserDeletingRow += new Telerik.WinControls.UI.GridViewRowCancelEventHandler(this.taskuriGridView_UserDeletingRow);
            this.taskuriGridView.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.taskuriGridView_CellValueChanged);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.CausesValidation = false;
            this.radSplitContainer1.Controls.Add(this.documentContainer1);
            this.radSplitContainer1.Controls.Add(this.toolTabStrip2);
            this.radSplitContainer1.IsCleanUpTarget = true;
            this.radSplitContainer1.Location = new System.Drawing.Point(5, 5);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(722, 335);
            this.radSplitContainer1.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 281);
            this.radSplitContainer1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 137);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.CausesValidation = false;
            this.documentContainer1.Controls.Add(this.documentTabStrip1);
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // documentTabStrip1
            // 
            this.documentTabStrip1.CanUpdateChildIndex = true;
            this.documentTabStrip1.CausesValidation = false;
            this.documentTabStrip1.Controls.Add(this.taskuriWindow);
            this.documentTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.documentTabStrip1.Name = "documentTabStrip1";
            // 
            // 
            // 
            this.documentTabStrip1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentTabStrip1.SelectedIndex = 0;
            this.documentTabStrip1.Size = new System.Drawing.Size(518, 335);
            this.documentTabStrip1.TabIndex = 0;
            this.documentTabStrip1.TabStop = false;
            // 
            // toolTabStrip2
            // 
            this.toolTabStrip2.CanUpdateChildIndex = true;
            this.toolTabStrip2.Controls.Add(this.adaugaTaskWindow);
            this.toolTabStrip2.Location = new System.Drawing.Point(522, 0);
            this.toolTabStrip2.Name = "toolTabStrip2";
            // 
            // 
            // 
            this.toolTabStrip2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip2.SelectedIndex = 0;
            this.toolTabStrip2.Size = new System.Drawing.Size(200, 335);
            this.toolTabStrip2.TabIndex = 1;
            this.toolTabStrip2.TabStop = false;
            // 
            // adaugaTaskWindow
            // 
            this.adaugaTaskWindow.Caption = null;
            this.adaugaTaskWindow.Controls.Add(this.adaugaTaskGroupBox);
            this.adaugaTaskWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.adaugaTaskWindow.Location = new System.Drawing.Point(1, 24);
            this.adaugaTaskWindow.Name = "adaugaTaskWindow";
            this.adaugaTaskWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.adaugaTaskWindow.Size = new System.Drawing.Size(198, 309);
            this.adaugaTaskWindow.Text = "AdaugaTask";
            // 
            // adaugaTaskGroupBox
            // 
            this.adaugaTaskGroupBox.Controls.Add(this.descriereTaskTextBox);
            this.adaugaTaskGroupBox.Controls.Add(this.adaugaTaskButton);
            this.adaugaTaskGroupBox.Controls.Add(this.cancelTaskButton);
            this.adaugaTaskGroupBox.Controls.Add(this.descriereTaskLabel);
            this.adaugaTaskGroupBox.Controls.Add(this.denumireTaskTextBox);
            this.adaugaTaskGroupBox.Controls.Add(this.denumireTaskLabel);
            this.adaugaTaskGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adaugaTaskGroupBox.Location = new System.Drawing.Point(0, 0);
            this.adaugaTaskGroupBox.Name = "adaugaTaskGroupBox";
            this.adaugaTaskGroupBox.Size = new System.Drawing.Size(198, 309);
            this.adaugaTaskGroupBox.TabIndex = 10;
            this.adaugaTaskGroupBox.TabStop = false;
            // 
            // descriereTaskTextBox
            // 
            this.descriereTaskTextBox.Location = new System.Drawing.Point(8, 93);
            this.descriereTaskTextBox.Name = "descriereTaskTextBox";
            this.descriereTaskTextBox.Size = new System.Drawing.Size(221, 221);
            this.descriereTaskTextBox.TabIndex = 2;
            // 
            // adaugaTaskButton
            // 
            this.adaugaTaskButton.Location = new System.Drawing.Point(152, 320);
            this.adaugaTaskButton.Name = "adaugaTaskButton";
            this.adaugaTaskButton.Size = new System.Drawing.Size(77, 24);
            this.adaugaTaskButton.TabIndex = 3;
            this.adaugaTaskButton.Text = "Adauga";
            this.adaugaTaskButton.Click += new System.EventHandler(this.adaugaTaskButton_Click);
            // 
            // cancelTaskButton
            // 
            this.cancelTaskButton.Location = new System.Drawing.Point(7, 320);
            this.cancelTaskButton.Name = "cancelTaskButton";
            this.cancelTaskButton.Size = new System.Drawing.Size(77, 24);
            this.cancelTaskButton.TabIndex = 4;
            this.cancelTaskButton.Text = "Cancel";
            this.cancelTaskButton.Click += new System.EventHandler(this.cancelTaskButton_Click);
            // 
            // descriereTaskLabel
            // 
            this.descriereTaskLabel.Location = new System.Drawing.Point(7, 69);
            this.descriereTaskLabel.Name = "descriereTaskLabel";
            this.descriereTaskLabel.Size = new System.Drawing.Size(53, 18);
            this.descriereTaskLabel.TabIndex = 1;
            this.descriereTaskLabel.Text = "Descriere";
            // 
            // denumireTaskTextBox
            // 
            this.denumireTaskTextBox.Location = new System.Drawing.Point(7, 43);
            this.denumireTaskTextBox.Name = "denumireTaskTextBox";
            this.denumireTaskTextBox.Size = new System.Drawing.Size(222, 20);
            this.denumireTaskTextBox.TabIndex = 1;
            // 
            // denumireTaskLabel
            // 
            this.denumireTaskLabel.Location = new System.Drawing.Point(7, 19);
            this.denumireTaskLabel.Name = "denumireTaskLabel";
            this.denumireTaskLabel.Size = new System.Drawing.Size(55, 18);
            this.denumireTaskLabel.TabIndex = 0;
            this.denumireTaskLabel.Text = "Denumire";
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CanUpdateChildIndex = true;
            this.toolTabStrip3.CausesValidation = false;
            this.toolTabStrip3.Controls.Add(this.controaleWindow);
            this.toolTabStrip3.Location = new System.Drawing.Point(5, 344);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(722, 63);
            this.toolTabStrip3.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 63);
            this.toolTabStrip3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -137);
            this.toolTabStrip3.TabIndex = 1;
            this.toolTabStrip3.TabStop = false;
            // 
            // controaleWindow
            // 
            this.controaleWindow.Caption = null;
            this.controaleWindow.Controls.Add(this.stopTimerButton);
            this.controaleWindow.Controls.Add(this.waitingBar);
            this.controaleWindow.Controls.Add(this.startTimerButton);
            this.controaleWindow.Controls.Add(this.reportButton);
            this.controaleWindow.Controls.Add(this.terminaTaskButton);
            this.controaleWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.controaleWindow.Location = new System.Drawing.Point(1, 24);
            this.controaleWindow.Name = "controaleWindow";
            this.controaleWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.controaleWindow.Size = new System.Drawing.Size(720, 37);
            this.controaleWindow.Text = "Controale";
            // 
            // reportButton
            // 
            this.reportButton.Location = new System.Drawing.Point(6, 8);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(110, 24);
            this.reportButton.TabIndex = 2;
            this.reportButton.Text = "Report";
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // terminaTaskButton
            // 
            this.terminaTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.terminaTaskButton.Location = new System.Drawing.Point(604, 8);
            this.terminaTaskButton.Name = "terminaTaskButton";
            this.terminaTaskButton.Size = new System.Drawing.Size(110, 24);
            this.terminaTaskButton.TabIndex = 1;
            this.terminaTaskButton.Text = "Termina Task";
            this.terminaTaskButton.Click += new System.EventHandler(this.terminaTaskButton_Click);
            // 
            // dockWindowPlaceholder1
            // 
            this.dockWindowPlaceholder1.AutoHideSize = new System.Drawing.Size(249, 237);
            this.dockWindowPlaceholder1.DockWindowName = "adaugaTaskWindow";
            this.dockWindowPlaceholder1.DockWindowText = "AdaugaTask";
            this.dockWindowPlaceholder1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dockWindowPlaceholder1.Location = new System.Drawing.Point(0, 0);
            this.dockWindowPlaceholder1.Name = "dockWindowPlaceholder1";
            this.dockWindowPlaceholder1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.dockWindowPlaceholder1.Size = new System.Drawing.Size(200, 200);
            this.dockWindowPlaceholder1.Text = "dockWindowPlaceholder1";
            // 
            // startTimerButton
            // 
            this.startTimerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startTimerButton.Location = new System.Drawing.Point(407, 8);
            this.startTimerButton.Name = "startTimerButton";
            this.startTimerButton.Size = new System.Drawing.Size(72, 24);
            this.startTimerButton.TabIndex = 3;
            this.startTimerButton.Text = "Start Timer";
            this.startTimerButton.Click += new System.EventHandler(this.startTimerButton_Click);
            // 
            // waitingBar
            // 
            this.waitingBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.waitingBar.Location = new System.Drawing.Point(485, 8);
            this.waitingBar.Name = "waitingBar";
            this.waitingBar.Size = new System.Drawing.Size(35, 24);
            this.waitingBar.TabIndex = 4;
            this.waitingBar.Text = "waitingBar";
            this.waitingBar.WaitingIndicatorSize = new System.Drawing.Size(5, 14);
            this.waitingBar.WaitingStep = 2;
            this.waitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Throbber;
            // 
            // stopTimerButton
            // 
            this.stopTimerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopTimerButton.Location = new System.Drawing.Point(526, 8);
            this.stopTimerButton.Name = "stopTimerButton";
            this.stopTimerButton.Size = new System.Drawing.Size(72, 24);
            this.stopTimerButton.TabIndex = 4;
            this.stopTimerButton.Text = "Stop Timer";
            this.stopTimerButton.Click += new System.EventHandler(this.stopTimerButton_Click);
            // 
            // parolaPanel
            // 
            this.parolaPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.parolaPanel.Controls.Add(this.parolaButton);
            this.parolaPanel.Controls.Add(this.parolaTextBox);
            this.parolaPanel.Controls.Add(this.parolaLabel);
            this.parolaPanel.Location = new System.Drawing.Point(154, 12);
            this.parolaPanel.Name = "parolaPanel";
            this.parolaPanel.Size = new System.Drawing.Size(303, 26);
            this.parolaPanel.TabIndex = 3;
            this.parolaPanel.Visible = false;
            // 
            // parolaLabel
            // 
            this.parolaLabel.Location = new System.Drawing.Point(3, 5);
            this.parolaLabel.Name = "parolaLabel";
            this.parolaLabel.Size = new System.Drawing.Size(68, 18);
            this.parolaLabel.TabIndex = 0;
            this.parolaLabel.Text = "Parola noua:";
            // 
            // parolaTextBox
            // 
            this.parolaTextBox.Location = new System.Drawing.Point(77, 4);
            this.parolaTextBox.Name = "parolaTextBox";
            this.parolaTextBox.PasswordChar = '●';
            this.parolaTextBox.Size = new System.Drawing.Size(113, 20);
            this.parolaTextBox.TabIndex = 1;
            this.parolaTextBox.UseSystemPasswordChar = true;
            // 
            // parolaButton
            // 
            this.parolaButton.Location = new System.Drawing.Point(196, 3);
            this.parolaButton.Name = "parolaButton";
            this.parolaButton.Size = new System.Drawing.Size(104, 21);
            this.parolaButton.TabIndex = 2;
            this.parolaButton.Text = "Reseteaza";
            this.parolaButton.Click += new System.EventHandler(this.passwordButton_Click);
            // 
            // reseteazaParolaMenuItem
            // 
            this.reseteazaParolaMenuItem.AccessibleDescription = "Reseteaza Parola";
            this.reseteazaParolaMenuItem.AccessibleName = "Reseteaza Parola";
            this.reseteazaParolaMenuItem.Name = "reseteazaParolaMenuItem";
            this.reseteazaParolaMenuItem.Text = "Reseteaza Parola";
            this.reseteazaParolaMenuItem.Click += new System.EventHandler(this.resetPasswordMenuItem_Click);
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 455);
            this.Controls.Add(this.mainDock);
            this.Controls.Add(this.radGroupBox1);
            this.MinimumSize = new System.Drawing.Size(740, 485);
            this.Name = "StudentForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StudentForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StudentForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDock)).EndInit();
            this.mainDock.ResumeLayout(false);
            this.taskuriWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            this.documentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).EndInit();
            this.documentTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).EndInit();
            this.toolTabStrip2.ResumeLayout(false);
            this.adaugaTaskWindow.ResumeLayout(false);
            this.adaugaTaskGroupBox.ResumeLayout(false);
            this.adaugaTaskGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaTaskButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelTaskButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            this.controaleWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminaTaskButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startTimerButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopTimerButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaPanel)).EndInit();
            this.parolaPanel.ResumeLayout(false);
            this.parolaPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parolaLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parolaButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem showMenuItem;
        private Telerik.WinControls.UI.RadLabel numeLabel;
        private Telerik.WinControls.UI.RadButton logoutButton;
        private Telerik.WinControls.UI.Docking.RadDock mainDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.DocumentWindow taskuriWindow;
        private Telerik.WinControls.UI.RadGridView taskuriGridView;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip documentTabStrip1;
        private Telerik.WinControls.UI.Docking.ToolWindow adaugaTaskWindow;
        private System.Windows.Forms.GroupBox adaugaTaskGroupBox;
        private Telerik.WinControls.UI.RadTextBoxControl descriereTaskTextBox;
        private Telerik.WinControls.UI.RadButton adaugaTaskButton;
        private Telerik.WinControls.UI.RadButton cancelTaskButton;
        private Telerik.WinControls.UI.RadLabel descriereTaskLabel;
        private Telerik.WinControls.UI.RadTextBox denumireTaskTextBox;
        private Telerik.WinControls.UI.RadLabel denumireTaskLabel;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem taskuriMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
        private Telerik.WinControls.UI.RadMenuItem adaugaTaskMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip2;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.ToolWindow controaleWindow;
        private Telerik.WinControls.UI.RadMenuItem controaleMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
        private Telerik.WinControls.UI.RadButton terminaTaskButton;
        private Telerik.WinControls.UI.RadButton reportButton;
        private Telerik.WinControls.UI.Docking.DockWindowPlaceholder dockWindowPlaceholder1;
        private Telerik.WinControls.UI.RadWaitingBar waitingBar;
        private Telerik.WinControls.UI.RadButton startTimerButton;
        private Telerik.WinControls.UI.RadButton stopTimerButton;
        private Telerik.WinControls.UI.RadPanel parolaPanel;
        private Telerik.WinControls.UI.RadLabel parolaLabel;
        private Telerik.WinControls.UI.RadButton parolaButton;
        private Telerik.WinControls.UI.RadTextBox parolaTextBox;
        private Telerik.WinControls.UI.RadMenuItem reseteazaParolaMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
    }
}
