﻿namespace EvidentaPractica
{
    partial class IndrumatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.Docking.AutoHideGroup autoHideGroup1 = new Telerik.WinControls.UI.Docking.AutoHideGroup();
            Telerik.WinControls.UI.Docking.AutoHideGroup autoHideGroup2 = new Telerik.WinControls.UI.Docking.AutoHideGroup();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.dockWindowPlaceholder1 = new Telerik.WinControls.UI.Docking.DockWindowPlaceholder();
            this.dockWindowPlaceholder2 = new Telerik.WinControls.UI.Docking.DockWindowPlaceholder();
            this.studentiGridView = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.showMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.studentiMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.taskuriMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.adaugaStudentMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.adaugaTaskMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.numeLabel = new Telerik.WinControls.UI.RadLabel();
            this.logoutButton = new Telerik.WinControls.UI.RadButton();
            this.mainDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.adaugaTaskWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.adaugaTaskGroupBox = new System.Windows.Forms.GroupBox();
            this.descriereTaskTextBox = new Telerik.WinControls.UI.RadTextBoxControl();
            this.adaugaTaskButton = new Telerik.WinControls.UI.RadButton();
            this.cancelTaskButton = new Telerik.WinControls.UI.RadButton();
            this.descriereTaskLabel = new Telerik.WinControls.UI.RadLabel();
            this.denumireTaskTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.denumireTaskLabel = new Telerik.WinControls.UI.RadLabel();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.studentiTabStrip = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.studentiWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.taskuriTabStrip = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.taskuriWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.taskuriGridView = new Telerik.WinControls.UI.RadGridView();
            this.adaugaStudentiTabStrip = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.adaugaStudentiWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.adaugaStudentGroupBox = new System.Windows.Forms.GroupBox();
            this.notaStudentDropDown = new Telerik.WinControls.UI.RadDropDownList();
            this.adaugaStudentButton = new Telerik.WinControls.UI.RadButton();
            this.cancelStudentButton = new Telerik.WinControls.UI.RadButton();
            this.notaStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.anStudentTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.anStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.institutieStudentTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.institutieStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.adresaStudentTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.cnpStudentTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.adresaStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.cnpStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.numeStudentTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.numeStudentLabel = new Telerik.WinControls.UI.RadLabel();
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.documentTabStrip2 = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.noteMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            ((System.ComponentModel.ISupportInitialize)(this.studentiGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentiGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDock)).BeginInit();
            this.mainDock.SuspendLayout();
            this.adaugaTaskWindow.SuspendLayout();
            this.adaugaTaskGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaTaskButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelTaskButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            this.documentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentiTabStrip)).BeginInit();
            this.studentiTabStrip.SuspendLayout();
            this.studentiWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriTabStrip)).BeginInit();
            this.taskuriTabStrip.SuspendLayout();
            this.taskuriWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaStudentiTabStrip)).BeginInit();
            this.adaugaStudentiTabStrip.SuspendLayout();
            this.adaugaStudentiWindow.SuspendLayout();
            this.adaugaStudentGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notaStudentDropDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaStudentButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelStudentButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notaStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anStudentTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.institutieStudentTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.institutieStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresaStudentTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpStudentTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresaStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeStudentTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeStudentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            this.toolTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dockWindowPlaceholder1
            // 
            this.dockWindowPlaceholder1.AutoHideSize = new System.Drawing.Size(247, 243);
            this.dockWindowPlaceholder1.DockWindowName = "adaugaStudentiWindow";
            this.dockWindowPlaceholder1.DockWindowText = "AdaugaStudent";
            this.dockWindowPlaceholder1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dockWindowPlaceholder1.Location = new System.Drawing.Point(0, 0);
            this.dockWindowPlaceholder1.Name = "dockWindowPlaceholder1";
            this.dockWindowPlaceholder1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.dockWindowPlaceholder1.Size = new System.Drawing.Size(200, 200);
            this.dockWindowPlaceholder1.Text = "dockWindowPlaceholder1";
            // 
            // dockWindowPlaceholder2
            // 
            this.dockWindowPlaceholder2.AutoHideSize = new System.Drawing.Size(246, 249);
            this.dockWindowPlaceholder2.DockWindowName = "adaugaTaskWindow";
            this.dockWindowPlaceholder2.DockWindowText = "AdaugaTask";
            this.dockWindowPlaceholder2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dockWindowPlaceholder2.Location = new System.Drawing.Point(0, 0);
            this.dockWindowPlaceholder2.Name = "dockWindowPlaceholder2";
            this.dockWindowPlaceholder2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.dockWindowPlaceholder2.Size = new System.Drawing.Size(200, 200);
            this.dockWindowPlaceholder2.Text = "dockWindowPlaceholder2";
            // 
            // studentiGridView
            // 
            this.studentiGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.studentiGridView.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.studentiGridView.MasterTemplate.AllowAddNewRow = false;
            this.studentiGridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.studentiGridView.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.studentiGridView.Name = "studentiGridView";
            this.studentiGridView.Size = new System.Drawing.Size(279, 161);
            this.studentiGridView.TabIndex = 0;
            this.studentiGridView.Text = "studentiGridView";
            this.studentiGridView.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.studentiGridView_CurrentRowChanged);
            this.studentiGridView.UserDeletingRow += new Telerik.WinControls.UI.GridViewRowCancelEventHandler(this.studentiGridView_UserDeletingRow);
            this.studentiGridView.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.studentiGridView_CellValueChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radMenu1);
            this.radGroupBox1.Controls.Add(this.numeLabel);
            this.radGroupBox1.Controls.Add(this.logoutButton);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Informatii";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(732, 43);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Informatii";
            // 
            // radMenu1
            // 
            this.radMenu1.Dock = System.Windows.Forms.DockStyle.None;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.showMenuItem});
            this.radMenu1.Location = new System.Drawing.Point(5, 16);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(325, 20);
            this.radMenu1.TabIndex = 2;
            this.radMenu1.Text = "radMenu1";
            // 
            // showMenuItem
            // 
            this.showMenuItem.AccessibleDescription = "Show";
            this.showMenuItem.AccessibleName = "Show";
            this.showMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuSeparatorItem1,
            this.studentiMenuItem,
            this.radMenuSeparatorItem2,
            this.taskuriMenuItem,
            this.radMenuSeparatorItem3,
            this.adaugaStudentMenuItem,
            this.radMenuSeparatorItem4,
            this.adaugaTaskMenuItem,
            this.radMenuSeparatorItem5,
            this.noteMenuItem,
            this.radMenuSeparatorItem6});
            this.showMenuItem.Name = "showMenuItem";
            this.showMenuItem.Text = "Deschide";
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // studentiMenuItem
            // 
            this.studentiMenuItem.AccessibleDescription = "Studenti Window";
            this.studentiMenuItem.AccessibleName = "Studenti Window";
            this.studentiMenuItem.Name = "studentiMenuItem";
            this.studentiMenuItem.Text = "Studenti";
            this.studentiMenuItem.Click += new System.EventHandler(this.studentiMenuItem_Click);
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // taskuriMenuItem
            // 
            this.taskuriMenuItem.AccessibleDescription = "Taskuri Window";
            this.taskuriMenuItem.AccessibleName = "Taskuri Window";
            this.taskuriMenuItem.Name = "taskuriMenuItem";
            this.taskuriMenuItem.Text = "Taskuri";
            this.taskuriMenuItem.Click += new System.EventHandler(this.taskuriMenuItem_Click);
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adaugaStudentMenuItem
            // 
            this.adaugaStudentMenuItem.AccessibleDescription = "Adauga Student Window";
            this.adaugaStudentMenuItem.AccessibleName = "Adauga Student Window";
            this.adaugaStudentMenuItem.Name = "adaugaStudentMenuItem";
            this.adaugaStudentMenuItem.Text = "Adauga Student";
            this.adaugaStudentMenuItem.Click += new System.EventHandler(this.adaugaMenuItem_Click_1);
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adaugaTaskMenuItem
            // 
            this.adaugaTaskMenuItem.AccessibleDescription = "Adauga Task";
            this.adaugaTaskMenuItem.AccessibleName = "Adauga Task";
            this.adaugaTaskMenuItem.Name = "adaugaTaskMenuItem";
            this.adaugaTaskMenuItem.Text = "Adauga Task";
            this.adaugaTaskMenuItem.Click += new System.EventHandler(this.adaugaTaskMenuItem_Click);
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numeLabel
            // 
            this.numeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numeLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.numeLabel.Location = new System.Drawing.Point(462, 16);
            this.numeLabel.Name = "numeLabel";
            this.numeLabel.Size = new System.Drawing.Size(115, 18);
            this.numeLabel.TabIndex = 1;
            this.numeLabel.Text = "Logat ca Indrumator";
            // 
            // logoutButton
            // 
            this.logoutButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.logoutButton.Location = new System.Drawing.Point(617, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(110, 26);
            this.logoutButton.TabIndex = 0;
            this.logoutButton.Text = "Logout";
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // mainDock
            // 
            this.mainDock.ActiveWindow = this.studentiWindow;
            this.mainDock.CausesValidation = false;
            this.mainDock.Controls.Add(this.documentContainer1);
            this.mainDock.Controls.Add(this.adaugaStudentiTabStrip);
            this.mainDock.Controls.Add(this.toolTabStrip1);
            this.mainDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDock.IsCleanUpTarget = true;
            this.mainDock.Location = new System.Drawing.Point(0, 43);
            this.mainDock.MainDocumentContainer = this.documentContainer1;
            this.mainDock.Name = "mainDock";
            // 
            // 
            // 
            this.mainDock.RootElement.MinSize = new System.Drawing.Size(0, 0);
            autoHideGroup1.Windows.Add(this.dockWindowPlaceholder1);
            autoHideGroup2.Windows.Add(this.dockWindowPlaceholder2);
            this.mainDock.SerializableAutoHideContainer.RightAutoHideGroups.Add(autoHideGroup1);
            this.mainDock.SerializableAutoHideContainer.RightAutoHideGroups.Add(autoHideGroup2);
            this.mainDock.Size = new System.Drawing.Size(732, 406);
            this.mainDock.TabIndex = 2;
            this.mainDock.TabStop = false;
            this.mainDock.Text = "mainDock";
            // 
            // adaugaTaskWindow
            // 
            this.adaugaTaskWindow.Caption = null;
            this.adaugaTaskWindow.Controls.Add(this.adaugaTaskGroupBox);
            this.adaugaTaskWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.adaugaTaskWindow.Location = new System.Drawing.Point(1, 24);
            this.adaugaTaskWindow.Name = "adaugaTaskWindow";
            this.adaugaTaskWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.adaugaTaskWindow.Size = new System.Drawing.Size(198, 370);
            this.adaugaTaskWindow.Text = "AdaugaTask";
            // 
            // adaugaTaskGroupBox
            // 
            this.adaugaTaskGroupBox.Controls.Add(this.descriereTaskTextBox);
            this.adaugaTaskGroupBox.Controls.Add(this.adaugaTaskButton);
            this.adaugaTaskGroupBox.Controls.Add(this.cancelTaskButton);
            this.adaugaTaskGroupBox.Controls.Add(this.descriereTaskLabel);
            this.adaugaTaskGroupBox.Controls.Add(this.denumireTaskTextBox);
            this.adaugaTaskGroupBox.Controls.Add(this.denumireTaskLabel);
            this.adaugaTaskGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adaugaTaskGroupBox.Location = new System.Drawing.Point(0, 0);
            this.adaugaTaskGroupBox.Name = "adaugaTaskGroupBox";
            this.adaugaTaskGroupBox.Size = new System.Drawing.Size(198, 370);
            this.adaugaTaskGroupBox.TabIndex = 9;
            this.adaugaTaskGroupBox.TabStop = false;
            // 
            // descriereTaskTextBox
            // 
            this.descriereTaskTextBox.Location = new System.Drawing.Point(8, 93);
            this.descriereTaskTextBox.Name = "descriereTaskTextBox";
            this.descriereTaskTextBox.Size = new System.Drawing.Size(221, 221);
            this.descriereTaskTextBox.TabIndex = 2;
            // 
            // adaugaTaskButton
            // 
            this.adaugaTaskButton.Location = new System.Drawing.Point(152, 320);
            this.adaugaTaskButton.Name = "adaugaTaskButton";
            this.adaugaTaskButton.Size = new System.Drawing.Size(77, 24);
            this.adaugaTaskButton.TabIndex = 3;
            this.adaugaTaskButton.Text = "Adauga";
            this.adaugaTaskButton.Click += new System.EventHandler(this.adaugaTaskButton_Click);
            // 
            // cancelTaskButton
            // 
            this.cancelTaskButton.Location = new System.Drawing.Point(7, 320);
            this.cancelTaskButton.Name = "cancelTaskButton";
            this.cancelTaskButton.Size = new System.Drawing.Size(77, 24);
            this.cancelTaskButton.TabIndex = 4;
            this.cancelTaskButton.Text = "Cancel";
            this.cancelTaskButton.Click += new System.EventHandler(this.cancelTaskButton_Click);
            // 
            // descriereTaskLabel
            // 
            this.descriereTaskLabel.Location = new System.Drawing.Point(7, 69);
            this.descriereTaskLabel.Name = "descriereTaskLabel";
            this.descriereTaskLabel.Size = new System.Drawing.Size(53, 18);
            this.descriereTaskLabel.TabIndex = 1;
            this.descriereTaskLabel.Text = "Descriere";
            // 
            // denumireTaskTextBox
            // 
            this.denumireTaskTextBox.Location = new System.Drawing.Point(7, 43);
            this.denumireTaskTextBox.Name = "denumireTaskTextBox";
            this.denumireTaskTextBox.Size = new System.Drawing.Size(222, 20);
            this.denumireTaskTextBox.TabIndex = 1;
            // 
            // denumireTaskLabel
            // 
            this.denumireTaskLabel.Location = new System.Drawing.Point(7, 19);
            this.denumireTaskLabel.Name = "denumireTaskLabel";
            this.denumireTaskLabel.Size = new System.Drawing.Size(55, 18);
            this.denumireTaskLabel.TabIndex = 0;
            this.denumireTaskLabel.Text = "Denumire";
            // 
            // documentContainer1
            // 
            this.documentContainer1.CausesValidation = false;
            this.documentContainer1.Controls.Add(this.studentiTabStrip);
            this.documentContainer1.Controls.Add(this.taskuriTabStrip);
            this.documentContainer1.Name = "documentContainer1";
            this.documentContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.AbsoluteSize = new System.Drawing.Size(487, 200);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-198, 0);
            // 
            // studentiTabStrip
            // 
            this.studentiTabStrip.CanUpdateChildIndex = true;
            this.studentiTabStrip.CausesValidation = false;
            this.studentiTabStrip.Controls.Add(this.studentiWindow);
            this.studentiTabStrip.Location = new System.Drawing.Point(0, 0);
            this.studentiTabStrip.Name = "studentiTabStrip";
            // 
            // 
            // 
            this.studentiTabStrip.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.studentiTabStrip.SelectedIndex = 0;
            this.studentiTabStrip.Size = new System.Drawing.Size(291, 196);
            this.studentiTabStrip.TabIndex = 1;
            this.studentiTabStrip.TabStop = false;
            // 
            // studentiWindow
            // 
            this.studentiWindow.Controls.Add(this.studentiGridView);
            this.studentiWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.studentiWindow.Location = new System.Drawing.Point(6, 29);
            this.studentiWindow.Name = "studentiWindow";
            this.studentiWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.studentiWindow.Size = new System.Drawing.Size(279, 161);
            this.studentiWindow.Text = "Studenti";
            // 
            // taskuriTabStrip
            // 
            this.taskuriTabStrip.CanUpdateChildIndex = true;
            this.taskuriTabStrip.CausesValidation = false;
            this.taskuriTabStrip.Controls.Add(this.taskuriWindow);
            this.taskuriTabStrip.Location = new System.Drawing.Point(0, 200);
            this.taskuriTabStrip.Name = "taskuriTabStrip";
            // 
            // 
            // 
            this.taskuriTabStrip.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.taskuriTabStrip.RootElement.Text = "";
            this.taskuriTabStrip.SelectedIndex = 0;
            this.taskuriTabStrip.Size = new System.Drawing.Size(291, 196);
            this.taskuriTabStrip.TabIndex = 0;
            this.taskuriTabStrip.TabStop = false;
            // 
            // taskuriWindow
            // 
            this.taskuriWindow.Controls.Add(this.taskuriGridView);
            this.taskuriWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.taskuriWindow.Location = new System.Drawing.Point(6, 29);
            this.taskuriWindow.Name = "taskuriWindow";
            this.taskuriWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.taskuriWindow.Size = new System.Drawing.Size(279, 161);
            this.taskuriWindow.Text = "Taskuri";
            // 
            // taskuriGridView
            // 
            this.taskuriGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskuriGridView.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.taskuriGridView.MasterTemplate.AllowAddNewRow = false;
            this.taskuriGridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.taskuriGridView.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.taskuriGridView.Name = "taskuriGridView";
            this.taskuriGridView.Size = new System.Drawing.Size(279, 161);
            this.taskuriGridView.TabIndex = 1;
            this.taskuriGridView.Text = "taskuriGridView";
            this.taskuriGridView.UserDeletingRow += new Telerik.WinControls.UI.GridViewRowCancelEventHandler(this.taskuriGridView_UserDeletingRow);
            this.taskuriGridView.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.taskuriGridView_CellValueChanged);
            // 
            // adaugaStudentiTabStrip
            // 
            this.adaugaStudentiTabStrip.CanUpdateChildIndex = true;
            this.adaugaStudentiTabStrip.Controls.Add(this.adaugaStudentiWindow);
            this.adaugaStudentiTabStrip.Location = new System.Drawing.Point(300, 5);
            this.adaugaStudentiTabStrip.Name = "adaugaStudentiTabStrip";
            // 
            // 
            // 
            this.adaugaStudentiTabStrip.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.adaugaStudentiTabStrip.SelectedIndex = 0;
            this.adaugaStudentiTabStrip.Size = new System.Drawing.Size(223, 396);
            this.adaugaStudentiTabStrip.SizeInfo.AbsoluteSize = new System.Drawing.Size(223, 200);
            this.adaugaStudentiTabStrip.SizeInfo.SplitterCorrection = new System.Drawing.Size(23, 0);
            this.adaugaStudentiTabStrip.TabIndex = 1;
            this.adaugaStudentiTabStrip.TabStop = false;
            // 
            // adaugaStudentiWindow
            // 
            this.adaugaStudentiWindow.Caption = null;
            this.adaugaStudentiWindow.Controls.Add(this.adaugaStudentGroupBox);
            this.adaugaStudentiWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.adaugaStudentiWindow.Location = new System.Drawing.Point(1, 24);
            this.adaugaStudentiWindow.Name = "adaugaStudentiWindow";
            this.adaugaStudentiWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.adaugaStudentiWindow.Size = new System.Drawing.Size(221, 370);
            this.adaugaStudentiWindow.Text = "AdaugaStudent";
            // 
            // adaugaStudentGroupBox
            // 
            this.adaugaStudentGroupBox.Controls.Add(this.notaStudentDropDown);
            this.adaugaStudentGroupBox.Controls.Add(this.adaugaStudentButton);
            this.adaugaStudentGroupBox.Controls.Add(this.cancelStudentButton);
            this.adaugaStudentGroupBox.Controls.Add(this.notaStudentLabel);
            this.adaugaStudentGroupBox.Controls.Add(this.anStudentTextBox);
            this.adaugaStudentGroupBox.Controls.Add(this.anStudentLabel);
            this.adaugaStudentGroupBox.Controls.Add(this.institutieStudentTextBox);
            this.adaugaStudentGroupBox.Controls.Add(this.institutieStudentLabel);
            this.adaugaStudentGroupBox.Controls.Add(this.adresaStudentTextBox);
            this.adaugaStudentGroupBox.Controls.Add(this.cnpStudentTextBox);
            this.adaugaStudentGroupBox.Controls.Add(this.adresaStudentLabel);
            this.adaugaStudentGroupBox.Controls.Add(this.cnpStudentLabel);
            this.adaugaStudentGroupBox.Controls.Add(this.numeStudentTextBox);
            this.adaugaStudentGroupBox.Controls.Add(this.numeStudentLabel);
            this.adaugaStudentGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adaugaStudentGroupBox.Location = new System.Drawing.Point(0, 0);
            this.adaugaStudentGroupBox.Name = "adaugaStudentGroupBox";
            this.adaugaStudentGroupBox.Size = new System.Drawing.Size(221, 370);
            this.adaugaStudentGroupBox.TabIndex = 0;
            this.adaugaStudentGroupBox.TabStop = false;
            // 
            // notaStudentDropDown
            // 
            this.notaStudentDropDown.Location = new System.Drawing.Point(7, 294);
            this.notaStudentDropDown.Name = "notaStudentDropDown";
            this.notaStudentDropDown.Size = new System.Drawing.Size(222, 20);
            this.notaStudentDropDown.TabIndex = 6;
            // 
            // adaugaStudentButton
            // 
            this.adaugaStudentButton.Location = new System.Drawing.Point(152, 320);
            this.adaugaStudentButton.Name = "adaugaStudentButton";
            this.adaugaStudentButton.Size = new System.Drawing.Size(77, 24);
            this.adaugaStudentButton.TabIndex = 7;
            this.adaugaStudentButton.Text = "Adauga";
            this.adaugaStudentButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // cancelStudentButton
            // 
            this.cancelStudentButton.Location = new System.Drawing.Point(7, 320);
            this.cancelStudentButton.Name = "cancelStudentButton";
            this.cancelStudentButton.Size = new System.Drawing.Size(77, 24);
            this.cancelStudentButton.TabIndex = 8;
            this.cancelStudentButton.Text = "Cancel";
            this.cancelStudentButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // notaStudentLabel
            // 
            this.notaStudentLabel.Location = new System.Drawing.Point(7, 269);
            this.notaStudentLabel.Name = "notaStudentLabel";
            this.notaStudentLabel.Size = new System.Drawing.Size(31, 18);
            this.notaStudentLabel.TabIndex = 3;
            this.notaStudentLabel.Text = "Nota";
            // 
            // anStudentTextBox
            // 
            this.anStudentTextBox.Location = new System.Drawing.Point(7, 243);
            this.anStudentTextBox.Name = "anStudentTextBox";
            this.anStudentTextBox.Size = new System.Drawing.Size(222, 20);
            this.anStudentTextBox.TabIndex = 5;
            // 
            // anStudentLabel
            // 
            this.anStudentLabel.Location = new System.Drawing.Point(7, 219);
            this.anStudentLabel.Name = "anStudentLabel";
            this.anStudentLabel.Size = new System.Drawing.Size(20, 18);
            this.anStudentLabel.TabIndex = 2;
            this.anStudentLabel.Text = "An";
            // 
            // institutieStudentTextBox
            // 
            this.institutieStudentTextBox.Location = new System.Drawing.Point(7, 193);
            this.institutieStudentTextBox.Name = "institutieStudentTextBox";
            this.institutieStudentTextBox.Size = new System.Drawing.Size(222, 20);
            this.institutieStudentTextBox.TabIndex = 4;
            // 
            // institutieStudentLabel
            // 
            this.institutieStudentLabel.Location = new System.Drawing.Point(7, 169);
            this.institutieStudentLabel.Name = "institutieStudentLabel";
            this.institutieStudentLabel.Size = new System.Drawing.Size(50, 18);
            this.institutieStudentLabel.TabIndex = 1;
            this.institutieStudentLabel.Text = "Institutie";
            // 
            // adresaStudentTextBox
            // 
            this.adresaStudentTextBox.Location = new System.Drawing.Point(7, 143);
            this.adresaStudentTextBox.Name = "adresaStudentTextBox";
            this.adresaStudentTextBox.Size = new System.Drawing.Size(222, 20);
            this.adresaStudentTextBox.TabIndex = 3;
            // 
            // cnpStudentTextBox
            // 
            this.cnpStudentTextBox.Location = new System.Drawing.Point(7, 93);
            this.cnpStudentTextBox.MaxLength = 13;
            this.cnpStudentTextBox.Name = "cnpStudentTextBox";
            this.cnpStudentTextBox.Size = new System.Drawing.Size(222, 20);
            this.cnpStudentTextBox.TabIndex = 2;
            // 
            // adresaStudentLabel
            // 
            this.adresaStudentLabel.Location = new System.Drawing.Point(7, 119);
            this.adresaStudentLabel.Name = "adresaStudentLabel";
            this.adresaStudentLabel.Size = new System.Drawing.Size(41, 18);
            this.adresaStudentLabel.TabIndex = 1;
            this.adresaStudentLabel.Text = "Adresa";
            // 
            // cnpStudentLabel
            // 
            this.cnpStudentLabel.Location = new System.Drawing.Point(7, 69);
            this.cnpStudentLabel.Name = "cnpStudentLabel";
            this.cnpStudentLabel.Size = new System.Drawing.Size(28, 18);
            this.cnpStudentLabel.TabIndex = 1;
            this.cnpStudentLabel.Text = "CNP";
            // 
            // numeStudentTextBox
            // 
            this.numeStudentTextBox.Location = new System.Drawing.Point(7, 43);
            this.numeStudentTextBox.Name = "numeStudentTextBox";
            this.numeStudentTextBox.Size = new System.Drawing.Size(222, 20);
            this.numeStudentTextBox.TabIndex = 1;
            // 
            // numeStudentLabel
            // 
            this.numeStudentLabel.Location = new System.Drawing.Point(7, 19);
            this.numeStudentLabel.Name = "numeStudentLabel";
            this.numeStudentLabel.Size = new System.Drawing.Size(37, 18);
            this.numeStudentLabel.TabIndex = 0;
            this.numeStudentLabel.Text = "Nume";
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.CanUpdateChildIndex = true;
            this.toolTabStrip1.Controls.Add(this.adaugaTaskWindow);
            this.toolTabStrip1.Location = new System.Drawing.Point(527, 5);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip1.SelectedIndex = 0;
            this.toolTabStrip1.Size = new System.Drawing.Size(200, 396);
            this.toolTabStrip1.TabIndex = 2;
            this.toolTabStrip1.TabStop = false;
            // 
            // documentTabStrip2
            // 
            this.documentTabStrip2.CanUpdateChildIndex = true;
            this.documentTabStrip2.CausesValidation = false;
            this.documentTabStrip2.Location = new System.Drawing.Point(378, 0);
            this.documentTabStrip2.Name = "documentTabStrip2";
            // 
            // 
            // 
            this.documentTabStrip2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentTabStrip2.Size = new System.Drawing.Size(374, 339);
            this.documentTabStrip2.TabIndex = 0;
            this.documentTabStrip2.TabStop = false;
            // 
            // noteMenuItem
            // 
            this.noteMenuItem.AccessibleDescription = "Note";
            this.noteMenuItem.AccessibleName = "Note";
            this.noteMenuItem.Name = "noteMenuItem";
            this.noteMenuItem.Text = "Note";
            this.noteMenuItem.Click += new System.EventHandler(this.noteMenuItem_Click);
            // 
            // radMenuSeparatorItem6
            // 
            this.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IndrumatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 449);
            this.Controls.Add(this.mainDock);
            this.Controls.Add(this.radGroupBox1);
            this.MinimumSize = new System.Drawing.Size(740, 479);
            this.Name = "IndrumatorForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IndrumatorForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IndrumatorForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.studentiGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentiGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDock)).EndInit();
            this.mainDock.ResumeLayout(false);
            this.adaugaTaskWindow.ResumeLayout(false);
            this.adaugaTaskGroupBox.ResumeLayout(false);
            this.adaugaTaskGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaTaskButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelTaskButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descriereTaskLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.denumireTaskLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            this.documentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.studentiTabStrip)).EndInit();
            this.studentiTabStrip.ResumeLayout(false);
            this.studentiWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.taskuriTabStrip)).EndInit();
            this.taskuriTabStrip.ResumeLayout(false);
            this.taskuriWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaStudentiTabStrip)).EndInit();
            this.adaugaStudentiTabStrip.ResumeLayout(false);
            this.adaugaStudentiWindow.ResumeLayout(false);
            this.adaugaStudentGroupBox.ResumeLayout(false);
            this.adaugaStudentGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notaStudentDropDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adaugaStudentButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelStudentButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notaStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anStudentTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.institutieStudentTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.institutieStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresaStudentTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpStudentTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresaStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnpStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeStudentTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeStudentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            this.toolTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView studentiGridView;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton logoutButton;
        private Telerik.WinControls.UI.RadLabel numeLabel;
        private Telerik.WinControls.UI.Docking.RadDock mainDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.DocumentWindow studentiWindow;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip documentTabStrip2;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip taskuriTabStrip;
        private Telerik.WinControls.UI.Docking.ToolWindow adaugaStudentiWindow;
        private System.Windows.Forms.GroupBox adaugaStudentGroupBox;
        private Telerik.WinControls.UI.RadLabel notaStudentLabel;
        private Telerik.WinControls.UI.RadTextBox anStudentTextBox;
        private Telerik.WinControls.UI.RadLabel anStudentLabel;
        private Telerik.WinControls.UI.RadTextBox institutieStudentTextBox;
        private Telerik.WinControls.UI.RadLabel institutieStudentLabel;
        private Telerik.WinControls.UI.RadTextBox adresaStudentTextBox;
        private Telerik.WinControls.UI.RadTextBox cnpStudentTextBox;
        private Telerik.WinControls.UI.RadLabel adresaStudentLabel;
        private Telerik.WinControls.UI.RadLabel cnpStudentLabel;
        private Telerik.WinControls.UI.RadTextBox numeStudentTextBox;
        private Telerik.WinControls.UI.RadLabel numeStudentLabel;
        private Telerik.WinControls.UI.RadButton adaugaStudentButton;
        private Telerik.WinControls.UI.RadButton cancelStudentButton;
        private Telerik.WinControls.UI.RadDropDownList notaStudentDropDown;
        private Telerik.WinControls.UI.Docking.ToolTabStrip adaugaStudentiTabStrip;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem showMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem studentiMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
        private Telerik.WinControls.UI.RadMenuItem taskuriMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip studentiTabStrip;
        private Telerik.WinControls.UI.Docking.DocumentWindow taskuriWindow;
        private Telerik.WinControls.UI.RadMenuItem adaugaStudentMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
        private Telerik.WinControls.UI.RadGridView taskuriGridView;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private Telerik.WinControls.UI.Docking.ToolWindow adaugaTaskWindow;
        private Telerik.WinControls.UI.RadMenuItem adaugaTaskMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
        private Telerik.WinControls.UI.Docking.DockWindowPlaceholder dockWindowPlaceholder1;
        private Telerik.WinControls.UI.Docking.DockWindowPlaceholder dockWindowPlaceholder2;
        private System.Windows.Forms.GroupBox adaugaTaskGroupBox;
        private Telerik.WinControls.UI.RadButton adaugaTaskButton;
        private Telerik.WinControls.UI.RadButton cancelTaskButton;
        private Telerik.WinControls.UI.RadLabel descriereTaskLabel;
        private Telerik.WinControls.UI.RadTextBox denumireTaskTextBox;
        private Telerik.WinControls.UI.RadLabel denumireTaskLabel;
        private Telerik.WinControls.UI.RadTextBoxControl descriereTaskTextBox;
        private Telerik.WinControls.UI.RadMenuItem noteMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem6;
    }
}
