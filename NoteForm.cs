﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Linq;
using System.Collections;
using System.Diagnostics;

namespace EvidentaPractica
{
    public partial class NoteForm : Telerik.WinControls.UI.RadForm
    {
        Dictionary<string, int> dictionarNume = new Dictionary<string, int>();
        Dictionary<int, string> dictionarNota = new Dictionary<int, string>();
        Stopwatch timer = new Stopwatch();

        public NoteForm()
        {
            InitializeComponent();
        }

        private void populeazaButton_Click(object sender, EventArgs e)
        {
            PopuleazaTabel();
        }

        private void PopuleazaTabel()
        {
            //AdaugaInregistrariInTabel();
            AfiseazaNoteInGridView();
        }

        private void AdaugaInregistrariInTabel()
        {
            var r = new Random();
            for (int i = 0; i < 50000; i++)
            {
                InsertInTabel(r.Next(1, 101), CreazaString(3));
            }
        }

        private void AfiseazaNoteInGridView()
        {
            noteGridView.DataSource = SelecteazaNote();
        }

        private DataTable SelecteazaNote()
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT nota,nume
                                                    FROM note", conexiune);
            SqlDataReader studentiSelectatiDataReader = comandaSql.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(studentiSelectatiDataReader);
            GestioneazaBD.InchideConexiune(conexiune);
            return dataTable;
        }

        private Dictionary<string, int> SelecteazaNoteInDictionarDupaNume()
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT nota,nume
                                                    FROM note", conexiune);
            SqlDataReader studentiSelectatiDataReader = comandaSql.ExecuteReader();

            while (studentiSelectatiDataReader.Read())
            {
                if (dictionarNume.ContainsKey(studentiSelectatiDataReader["nume"].ToString()))
                {
                    continue;
                }
                else
                {
                    dictionarNume.Add(studentiSelectatiDataReader["nume"].ToString(), (int)studentiSelectatiDataReader["nota"]);
                }
            }

            return dictionarNume;
        }

        private Dictionary<int, string> SelecteazaNoteInDictionarDupaNota()
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT nota,nume
                                                    FROM note", conexiune);
            SqlDataReader studentiSelectatiDataReader = comandaSql.ExecuteReader();

            while (studentiSelectatiDataReader.Read())
            {
                if (dictionarNota.ContainsKey((int)studentiSelectatiDataReader["nota"]))
                {
                    continue;
                }
                else
                {
                    dictionarNota.Add((int)studentiSelectatiDataReader["nota"], studentiSelectatiDataReader["nume"].ToString());
                }
            }

            return dictionarNota;
        }

        private void InsertInTabel(int nota, string nume)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"INSERT INTO note
                                                    VALUES (@nota,@nume)", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@nota", nota));
            comandaSql.Parameters.Add(new SqlParameter("@nume", nume));

            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        private string CreazaString(int stringLength)
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
            char[] chars = new char[stringLength];
            var rd = new Random();

            for (int i = 0; i < stringLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        private void eliminaNotaDublicataButton_Click(object sender, EventArgs e)
        {
            
            timer.Start();
            SelecteazaNoteInDictionarDupaNota();
            noteGridView.DataSource = dictionarNota;
            timer.Stop();
            MessageBox.Show((timer.Elapsed.TotalSeconds).ToString());
            timer.Reset();
            
        }

        private void eliminaNumeDublicatButton_Click(object sender, EventArgs e)
        {
            
            timer.Start();
            SelecteazaNoteInDictionarDupaNume();
            noteGridView.DataSource = dictionarNume;
            timer.Stop();
            MessageBox.Show((timer.Elapsed.TotalSeconds).ToString());
            timer.Reset();
            
        }
    }
}
