﻿namespace EvidentaPractica
{
    partial class NoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.populeazaButton = new Telerik.WinControls.UI.RadButton();
            this.noteGridView = new Telerik.WinControls.UI.RadGridView();
            this.eliminaNumeDublicatButton = new Telerik.WinControls.UI.RadButton();
            this.eliminaNotaDublicataButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.populeazaButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eliminaNumeDublicatButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eliminaNotaDublicataButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // populeazaButton
            // 
            this.populeazaButton.Location = new System.Drawing.Point(281, 12);
            this.populeazaButton.Name = "populeazaButton";
            this.populeazaButton.Size = new System.Drawing.Size(110, 24);
            this.populeazaButton.TabIndex = 0;
            this.populeazaButton.Text = "Populeaza";
            this.populeazaButton.Click += new System.EventHandler(this.populeazaButton_Click);
            // 
            // noteGridView
            // 
            this.noteGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.noteGridView.Location = new System.Drawing.Point(0, 42);
            // 
            // 
            // 
            this.noteGridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.noteGridView.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.noteGridView.Name = "noteGridView";
            this.noteGridView.Size = new System.Drawing.Size(403, 248);
            this.noteGridView.TabIndex = 1;
            this.noteGridView.Text = "radGridView1";
            // 
            // eliminaNumeDublicatButton
            // 
            this.eliminaNumeDublicatButton.Location = new System.Drawing.Point(49, 12);
            this.eliminaNumeDublicatButton.Name = "eliminaNumeDublicatButton";
            this.eliminaNumeDublicatButton.Size = new System.Drawing.Size(110, 24);
            this.eliminaNumeDublicatButton.TabIndex = 2;
            this.eliminaNumeDublicatButton.Text = "Elimina dupa nume";
            this.eliminaNumeDublicatButton.Click += new System.EventHandler(this.eliminaNumeDublicatButton_Click);
            // 
            // eliminaNotaDublicataButton
            // 
            this.eliminaNotaDublicataButton.Location = new System.Drawing.Point(165, 12);
            this.eliminaNotaDublicataButton.Name = "eliminaNotaDublicataButton";
            this.eliminaNotaDublicataButton.Size = new System.Drawing.Size(110, 24);
            this.eliminaNotaDublicataButton.TabIndex = 3;
            this.eliminaNotaDublicataButton.Text = "Elimina dupa nota";
            this.eliminaNotaDublicataButton.Click += new System.EventHandler(this.eliminaNotaDublicataButton_Click);
            // 
            // NoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 290);
            this.Controls.Add(this.eliminaNotaDublicataButton);
            this.Controls.Add(this.eliminaNumeDublicatButton);
            this.Controls.Add(this.noteGridView);
            this.Controls.Add(this.populeazaButton);
            this.Name = "NoteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "NoteForm";
            ((System.ComponentModel.ISupportInitialize)(this.populeazaButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eliminaNumeDublicatButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eliminaNotaDublicataButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton populeazaButton;
        private Telerik.WinControls.UI.RadGridView noteGridView;
        private Telerik.WinControls.UI.RadButton eliminaNumeDublicatButton;
        private Telerik.WinControls.UI.RadButton eliminaNotaDublicataButton;
    }
}
