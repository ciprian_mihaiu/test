﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidentaPractica.Servicii
{
    static class UserServices
    {

        internal static List<String> GetNumeUtilizatori()
        {
            List<String> ListNumeUtilizatori = new List<string>();
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT nume 
                                                    FROM users", conexiune);
            SqlDataReader numeUtilizatoriDataReader = comandaSql.ExecuteReader();
            while (numeUtilizatoriDataReader.Read())
            {
                ListNumeUtilizatori.Add(numeUtilizatoriDataReader["nume"].ToString());
            }
            GestioneazaBD.InchideConexiune(conexiune);
            return ListNumeUtilizatori;
        }

        internal static string CripteazaParola(string parola)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(parola);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);
            return hash;
        }

        internal static bool UtilizatorValid(string nume, string parola)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            var comandaSql = new SqlCommand(@"SELECT count(1) 
                                            FROM users 
                                            WHERE nume=@numeUtilizator 
                                                AND parola=@parolaUtilizator", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@numeUtilizator", nume));
            comandaSql.Parameters.Add(new SqlParameter("@parolaUtilizator", parola));
            bool eUtilizatorValid = (int)comandaSql.ExecuteScalar() != 0;
            GestioneazaBD.InchideConexiune(conexiune);
            return eUtilizatorValid;
        }

        internal static Constante.TipuriUtilizator TipUtilizator(string numeUtilizator)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT idRol 
                                            FROM users 
                                            WHERE nume=@numeUtilizator", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@numeUtilizator", numeUtilizator));
            SqlDataReader utilizatorDataReader = comandaSql.ExecuteReader();
            utilizatorDataReader.Read();
            int tipUtilizator = (int)utilizatorDataReader.GetInt32(0);
            GestioneazaBD.InchideConexiune(conexiune);
            return (Constante.TipuriUtilizator)tipUtilizator;
        }

        internal static DataTable SelecteazaStudenti()
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT idUser,nume as Nume,cnp as CNP,adresa as Adresa,institutie as Institutie,an as An,nota as Nota
                                            FROM users 
                                            WHERE idRol=2", conexiune);
            SqlDataReader studentiSelectatiDataReader = comandaSql.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(studentiSelectatiDataReader);
            GestioneazaBD.InchideConexiune(conexiune);
            return dataTable;
        }

        internal static void UpdateStudentSelectat(StudentEntity student, int idUser)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"UPDATE users 
                                                    SET nota=@notaStudent ,nume=@numeStudent,cnp=@cnpStudent,adresa=@adresaStudent,institutie=@institutieStudent,an=@anStudent 
                                                    WHERE idUser=" + idUser, conexiune);
            IncarcaParametriStudent(student, comandaSql);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static void DeleteStudent(int idUser)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"DELETE 
                                                    FROM users 
                                                    WHERE idUser=" + idUser, conexiune);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static void DeleteTaskuriStudent(int idStudent)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"DELETE 
                                                    FROM taskuri 
                                                    WHERE idUser=" + idStudent, conexiune);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static void InsertStudent(StudentEntity student)
        {
            string parola = UserServices.CripteazaParola("parola");
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"INSERT 
                                            INTO users(nume,cnp,adresa,institutie,an,idRol,parola,nota) 
                                            VALUES(@numeStudent,@cnpStudent,@adresaStudent,@institutieStudent,@anStudent,2,'" + parola + "',@notaStudent)", conexiune);
            IncarcaParametriStudent(student, comandaSql);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        private static void IncarcaParametriStudent(StudentEntity student, SqlCommand comandaSql)
        {
            comandaSql.Parameters.Add(new SqlParameter("@numeStudent", student.Nume));
            comandaSql.Parameters.Add(new SqlParameter("@cnpStudent", student.Cnp));
            comandaSql.Parameters.Add(new SqlParameter("@adresaStudent", student.Adresa));
            comandaSql.Parameters.Add(new SqlParameter("@institutieStudent", student.Institutie));
            comandaSql.Parameters.Add(new SqlParameter("@anStudent", student.An));
            comandaSql.Parameters.Add(new SqlParameter("@notaStudent", student.Nota));
        }

        internal static bool VerificaDacaExistaUserCuAcelasiNume(string nume)
        {
            string numeUtilizator = "";
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT * 
                                                    FROM users 
                                                    WHERE nume=@numeUser", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@numeUser", nume));

            SqlDataReader utilizatorSelectat = comandaSql.ExecuteReader();
            if (utilizatorSelectat.Read())
            {
                numeUtilizator = utilizatorSelectat["nume"].ToString();
            }
            GestioneazaBD.InchideConexiune(conexiune);
            return numeUtilizator != string.Empty ? true : false;
        }

        internal static int GetIdStudentByName(string nume)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT idUser
                                                    FROM users
                                                    WHERE nume='" + nume + "'", conexiune);
            SqlDataReader idUtilizatorSelectat = comandaSql.ExecuteReader();
            idUtilizatorSelectat.Read();
            int idUtilizator = (int)idUtilizatorSelectat["idUser"];
            GestioneazaBD.InchideConexiune(conexiune);
            return idUtilizator;
        }

        internal static void ReseteazaParola(int idStudent, string p)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"UPDATE users 
                                                    SET parola=@parolaNoua 
                                                    WHERE idUser='" + idStudent + "'", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@parolaNoua", UserServices.CripteazaParola(p)));
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }
    }
}
