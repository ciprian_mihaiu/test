﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidentaPractica.Servicii
{
    static class TaskServices
    {
        internal static DataTable SelecteazaTaskuriUtilizator(int idStudentSelectat)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT idTask,idUser,denumireTask as Denumire,(tipStatus.tipStatus) as Status,dataCrearii as Data,timpLucrat as TimpLucrat,notitaTask as Descriere
                                                    FROM taskuri
                                                    JOIN tipStatus
                                                    ON taskuri.idStatus = tipStatus.idStatus
                                                    WHERE idUser=" + idStudentSelectat, conexiune);
            SqlDataReader taskuriSelectateDataReader = comandaSql.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(taskuriSelectateDataReader);
            GestioneazaBD.InchideConexiune(conexiune);
            return dataTable;
        }

        internal static void UpdateTask(TaskEntity task, int idTask)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"UPDATE taskuri 
                                                    SET denumireTask=@denumireTask,notitaTask=@descriereTask 
                                                    WHERE idTask='" + idTask + "'", conexiune);
            comandaSql.Parameters.Add(new SqlParameter("@denumireTask", task.Denumire));
            comandaSql.Parameters.Add(new SqlParameter("@descriereTask", task.Descriere));
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static void DeleteTask(int idTask)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"DELETE 
                                                    FROM taskuri 
                                                    WHERE idTask=" + idTask, conexiune);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static void InsertTask(TaskEntity task, int idUser)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"INSERT INTO taskuri(idUser,denumireTask,idStatus,dataCrearii,timpLucrat,notitaTask) 
                                                    VALUES('" + idUser + "',@denumire,@status,@dataCreare,@timpLucrat,@descriere)", conexiune);
            IncarcaParametriTask(task, comandaSql);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        private static void IncarcaParametriTask(TaskEntity task, SqlCommand comandaSql)
        {
            comandaSql.Parameters.Add(new SqlParameter("@denumire", task.Denumire));
            comandaSql.Parameters.Add(new SqlParameter("@status", task.Status));
            comandaSql.Parameters.Add(new SqlParameter("@dataCreare", task.DataCreare));
            comandaSql.Parameters.Add(new SqlParameter("@timpLucrat", task.TimpLucrat));
            comandaSql.Parameters.Add(new SqlParameter("@descriere", task.Descriere));
        }

        internal static void UpdateStatus(int idTask)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"UPDATE taskuri 
                                                    SET idStatus=2 
                                                    WHERE idTask=" + idTask, conexiune);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }

        internal static string SelecteazaTimpLucratTask(int idTask)
        {
            string timpLucrat = "";
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT timpLucrat 
                                                    FROM taskuri 
                                                    WHERE idTask=" + idTask, conexiune);
            SqlDataReader timpLucratTask = comandaSql.ExecuteReader();
            timpLucratTask.Read();
            timpLucrat = timpLucratTask["timpLucrat"].ToString();
            GestioneazaBD.InchideConexiune(conexiune);
            return timpLucrat;
        }

        internal static void UpdateTimpLucratTask(int idTask, string timpLucratTotal)
        {
            SqlConnection conexiune = GestioneazaBD.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"UPDATE taskuri 
                                                    SET timpLucrat='" + timpLucratTotal + @"' 
                                                    WHERE idTask=" + idTask, conexiune);
            comandaSql.ExecuteNonQuery();
            GestioneazaBD.InchideConexiune(conexiune);
        }
    }
}
