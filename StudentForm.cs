﻿using EvidentaPractica.Servicii;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI.Docking;

namespace EvidentaPractica
{
    public partial class StudentForm : Telerik.WinControls.UI.RadForm
    {
        LoginForm formaLogin;
        ReportUser formaReport;
        TaskEntity task = new TaskEntity();
        int idStudent;
        Stopwatch timer = new Stopwatch();
        public StudentForm(string nume)
        {
            InitializeComponent();
            numeLabel.Text += nume;
            idStudent = UserServices.GetIdStudentByName(nume);
            AfiseazaTaskuriStudent();
            ModificaFerestrele();
        }

        private void ModificaFerestrele()
        {
            taskuriWindow.CloseAction = DockWindowCloseAction.Hide;
            adaugaTaskWindow.CloseAction = DockWindowCloseAction.Hide;
            controaleWindow.CloseAction = DockWindowCloseAction.Hide;
        }

        private void AfiseazaTaskuriStudent()
        {
            taskuriGridView.DataSource = TaskServices.SelecteazaTaskuriUtilizator(idStudent);
            ModificaTaskuriGridView();
        }

        private void ModificaTaskuriGridView()
        {
            taskuriGridView.Columns["idUser"].IsVisible = false;
            taskuriGridView.Columns["idTask"].IsVisible = false;
            taskuriGridView.Columns["TimpLucrat"].BestFit();
            taskuriGridView.Columns["Data"].BestFit();
            taskuriGridView.Columns["Status"].BestFit();
            taskuriGridView.Columns["Data"].ReadOnly = true;
            taskuriGridView.Columns["TimpLucrat"].ReadOnly = true;
            taskuriGridView.Columns["Status"].ReadOnly = true;
        }

        private void StudentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            DeschideFormaLogin();
        }

        private void DeschideFormaLogin()
        {
            formaLogin = new LoginForm();
            this.Hide();
            formaLogin.ShowDialog();
        }

        private void taskuriMenuItem_Click(object sender, EventArgs e)
        {
            taskuriWindow.Show();
        }

        private void adaugaTaskMenuItem_Click(object sender, EventArgs e)
        {
            adaugaTaskWindow.Show();
        }

        private void taskuriGridView_UserDeletingRow(object sender, Telerik.WinControls.UI.GridViewRowCancelEventArgs e)
        {
            int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
            TaskServices.DeleteTask(idTask);
        }

        private void IncarcaEntitateaTaskDinForma()
        {
            task.Denumire = denumireTaskTextBox.Text;
            task.Descriere = descriereTaskTextBox.Text;
            task.Status = 1;
            task.TimpLucrat = "00:00:00";
            task.DataCreare = GestioneazaBD.GetTimeStamp(DateTime.Now);
        }

        private void GolesteFormaDeAdaugareTask()
        {
            denumireTaskTextBox.Text = string.Empty;
            descriereTaskTextBox.Text = string.Empty;
        }

        private void adaugaTaskButton_Click(object sender, EventArgs e)
        {
            if (ValideazaFormaAdaugareTask())
            {
                IncarcaEntitateaTaskDinForma();
                TaskServices.InsertTask(task, idStudent);
            }
            else
            {
                MessageBox.Show("Toate campurile sunt obligatorii!");
            }
            GolesteFormaDeAdaugareTask();
            AfiseazaTaskuriStudent();
        }

        private bool ValideazaFormaAdaugareTask()
        {
            if (string.IsNullOrWhiteSpace(denumireTaskTextBox.Text) || string.IsNullOrWhiteSpace(descriereTaskTextBox.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cancelTaskButton_Click(object sender, EventArgs e)
        {
            GolesteFormaDeAdaugareTask();
        }

        private void taskuriGridView_CellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (ValideazaTaskuriGridViewPentruEditare())
            {
                IncarcaEntitateaTaskDinGridView();
                int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
                TaskServices.UpdateTask(task, idTask);
            }
        }

        private bool ValideazaTaskuriGridViewPentruEditare()
        {
            if (taskuriGridView.CurrentRow.Cells["Denumire"].Value.ToString() == string.Empty || taskuriGridView.CurrentRow.Cells["Descriere"].Value.ToString() == string.Empty)
            {
                MessageBox.Show("Eroare!, Lipsa valoare.");
                AfiseazaTaskuriStudent();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void IncarcaEntitateaTaskDinGridView()
        {
            task.Denumire = taskuriGridView.CurrentRow.Cells["Denumire"].Value.ToString();
            task.Descriere = taskuriGridView.CurrentRow.Cells["Descriere"].Value.ToString();
        }

        private void controaleMenuItem_Click(object sender, EventArgs e)
        {
            controaleWindow.Show();
        }

        private void terminaTaskButton_Click(object sender, EventArgs e)
        {
            if (!(taskuriGridView.Rows.Count < 1) && !waitingBar.IsWaiting && taskuriGridView.CurrentRow.Cells["Status"].Value.ToString() != "Terminat")
            {
                if (MessageBox.Show("Finish?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
                    TaskServices.UpdateStatus(idTask);
                    AfiseazaTaskuriStudent();
                }
            }
        }

        private void reportButton_Click(object sender, EventArgs e)
        {
            formaReport = new ReportUser(idStudent);
            formaReport.ShowDialog();
        }

        private void startTimerButton_Click(object sender, EventArgs e)
        {
            if (!waitingBar.IsWaiting && taskuriGridView.Rows.Count > 0)
            {
                if (taskuriGridView.CurrentRow.Cells["Status"].Value.ToString() != "Terminat")
                {
                    timer.Start();
                    waitingBar.StartWaiting();
                    taskuriGridView.Enabled = false;
                }
            }
        }

        private void stopTimerButton_Click(object sender, EventArgs e)
        {
            if (waitingBar.IsWaiting)
            {
                waitingBar.StopWaiting();
                timer.Stop();
                UpdateTimpLucratTask();
                timer.Reset();
                taskuriGridView.Enabled = true;
                AfiseazaTaskuriStudent();
            }
        }

        private void UpdateTimpLucratTask()
        {
            try
            {
                int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
                string timpLucratTotal = CalculeazaTimpTotalLucrat(idTask);
                TaskServices.UpdateTimpLucratTask(idTask, timpLucratTotal);
            }
            catch (Exception)
            {
                return;
            }
        }

        private string CalculeazaTimpTotalLucrat(int idTask)
        {
            string timpLucrat = TaskServices.SelecteazaTimpLucratTask(idTask);
            string timpLucratTotal = (TimeSpan.Parse(((TimeSpan)(timer.Elapsed)).ToString(@"hh\:mm\:ss")) + TimeSpan.Parse(timpLucrat)).ToString();
            return timpLucratTotal;
        }

        private void resetPasswordMenuItem_Click(object sender, EventArgs e)
        {
            parolaPanel.Visible = true;
            parolaTextBox.Focus();
        }

        private void passwordButton_Click(object sender, EventArgs e)
        {
            if (parolaTextBox.Text == string.Empty) { return; }
            UserServices.ReseteazaParola(idStudent, parolaTextBox.Text);
            parolaTextBox.Text = string.Empty;
            parolaPanel.Visible = false;
        }
    }
}
