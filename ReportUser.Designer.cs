﻿namespace EvidentaPractica
{
    partial class ReportUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.taskuriBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ReportsDataSet = new EvidentaPractica.ReportsDataSet();
            this.reportUserViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.taskuriTableAdapter = new EvidentaPractica.ReportsDataSetTableAdapters.taskuriTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.taskuriBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // taskuriBindingSource
            // 
            this.taskuriBindingSource.DataMember = "taskuri";
            this.taskuriBindingSource.DataSource = this.ReportsDataSet;
            // 
            // ReportsDataSet
            // 
            this.ReportsDataSet.DataSetName = "ReportsDataSet";
            this.ReportsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportUserViewer
            // 
            this.reportUserViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "ReportUserDataSet";
            reportDataSource2.Value = this.taskuriBindingSource;
            this.reportUserViewer.LocalReport.DataSources.Add(reportDataSource2);
            this.reportUserViewer.LocalReport.ReportEmbeddedResource = "EvidentaPractica.ReportUser.rdlc";
            this.reportUserViewer.Location = new System.Drawing.Point(0, 0);
            this.reportUserViewer.Name = "reportUserViewer";
            this.reportUserViewer.Size = new System.Drawing.Size(732, 455);
            this.reportUserViewer.TabIndex = 0;
            this.reportUserViewer.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
            // 
            // taskuriTableAdapter
            // 
            this.taskuriTableAdapter.ClearBeforeFill = true;
            // 
            // ReportUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 455);
            this.Controls.Add(this.reportUserViewer);
            this.MinimumSize = new System.Drawing.Size(740, 485);
            this.Name = "ReportUser";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportUser";
            this.Load += new System.EventHandler(this.ReportUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.taskuriBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportUserViewer;
        private System.Windows.Forms.BindingSource taskuriBindingSource;
        private ReportsDataSet ReportsDataSet;
        private ReportsDataSetTableAdapters.taskuriTableAdapter taskuriTableAdapter;
    }
}
