﻿using EvidentaPractica.Servicii;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;

namespace EvidentaPractica
{
    public partial class IndrumatorForm : Telerik.WinControls.UI.RadForm
    {
        LoginForm formaLogin;
        StudentEntity student = new StudentEntity();
        TaskEntity task = new TaskEntity();
        List<int> valoriNote = new List<int>(11);

        public IndrumatorForm()
        {
            InitializeComponent();
            AfiseazaStudentiInGridView();
            ModificaFerestrele();
            PopuleazaListaDeNote();
        }

        private void PopuleazaListaDeNote()
        {
            for (int i = 0; i < 11; i++)
            {
                valoriNote.Add(i);
            }
            notaStudentDropDown.DataSource = valoriNote;
        }

        private void ModificaFerestrele()
        {
            studentiWindow.CloseAction = DockWindowCloseAction.Hide;
            adaugaStudentiWindow.CloseAction = DockWindowCloseAction.Hide;
            taskuriWindow.CloseAction = DockWindowCloseAction.Hide;
            adaugaTaskWindow.CloseAction = DockWindowCloseAction.Hide;
        }

        private void AfiseazaStudentiInGridView()
        {
            studentiGridView.DataSource = UserServices.SelecteazaStudenti();
            studentiGridView.Columns["idUser"].IsVisible = false;
            studentiGridView.Columns["Adresa"].BestFit();
        }

        private void IndrumatorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            DeschideFormaLogin();
        }

        private void DeschideFormaLogin()
        {
            formaLogin = new LoginForm();
            this.Hide();
            formaLogin.ShowDialog();
        }

        private void IncarcaEntitateaStudentDinGridView()
        {
            try
            {
                student.Nume = studentiGridView.CurrentRow.Cells["Nume"].Value.ToString();
                student.Cnp = studentiGridView.CurrentRow.Cells["CNP"].Value.ToString();
                student.Adresa = studentiGridView.CurrentRow.Cells["Adresa"].Value.ToString();
                student.Institutie = studentiGridView.CurrentRow.Cells["Institutie"].Value.ToString();
                student.An = Convert.ToInt32(studentiGridView.CurrentRow.Cells["An"].Value);
                student.Nota = Convert.ToInt32(studentiGridView.CurrentRow.Cells["Nota"].Value);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return;
            }
        }

        private void studentiGridView_CellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (ValideazaGridViewStudentiPentruEditare())
            {
                IncarcaEntitateaStudentDinGridView();
                int idUser = (int)studentiGridView.CurrentRow.Cells["idUser"].Value;
                UserServices.UpdateStudentSelectat(student, idUser);
            }
        }

        private bool ValideazaGridViewStudentiPentruEditare()
        {
            if (studentiGridView.CurrentRow.Cells["CNP"].Value.ToString().Length > 13 || Convert.ToInt32(studentiGridView.CurrentRow.Cells["Nota"].Value) > 10 || Convert.ToInt32(studentiGridView.CurrentRow.Cells["Nota"].Value) < 0)
            {
                MessageBox.Show("Valoare eronata!");
                AfiseazaStudentiInGridView();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void studentiGridView_UserDeletingRow(object sender, Telerik.WinControls.UI.GridViewRowCancelEventArgs e)
        {
            int idUser = (int)studentiGridView.CurrentRow.Cells["idUser"].Value;
            UserServices.DeleteStudent(idUser);
            UserServices.DeleteTaskuriStudent(idUser);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (ValideazaFormaAdaugareStudent())
            {
                IncarcaEntitateaStudentDinForma();
                if (!UserServices.VerificaDacaExistaUserCuAcelasiNume(student.Nume))
                {
                    UserServices.InsertStudent(student);
                    GolesteFormaDeAdaugareStudent();
                    AfiseazaStudentiInGridView();
                }
                else
                {
                    MessageBox.Show("Nume utilizator indisponibil!");
                }
            }
        }

        private void GolesteFormaDeAdaugareStudent()
        {
            numeStudentTextBox.Text = string.Empty;
            cnpStudentTextBox.Text = string.Empty;
            adresaStudentTextBox.Text = string.Empty;
            institutieStudentTextBox.Text = string.Empty;
            anStudentTextBox.Text = string.Empty;
        }

        private void IncarcaEntitateaStudentDinForma()
        {
            try
            {
                student.Nume = numeStudentTextBox.Text;
                student.Cnp = cnpStudentTextBox.Text;
                student.Adresa = adresaStudentTextBox.Text;
                student.Institutie = institutieStudentTextBox.Text;
                student.An = Convert.ToInt32(anStudentTextBox.Text);
                student.Nota = Convert.ToInt32(notaStudentDropDown.SelectedValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return;
            }

        }

        private void studentiMenuItem_Click(object sender, EventArgs e)
        {
            studentiWindow.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            GolesteFormaDeAdaugareStudent();
        }

        private void adaugaMenuItem_Click_1(object sender, EventArgs e)
        {
            adaugaStudentiWindow.Show();
        }

        private void taskuriMenuItem_Click(object sender, EventArgs e)
        {
            taskuriWindow.Show();
        }

        private void AfiseazaTaskuriStudent()
        {
            if (IsEmptyStudentiGridView())
            {
                taskuriGridView.DataSource = null;
                return;
            }
            int idStudentSelectat = Convert.ToInt32(studentiGridView.CurrentRow.Cells["idUser"].Value);

            taskuriGridView.DataSource = TaskServices.SelecteazaTaskuriUtilizator(idStudentSelectat);
            ModificaTaskuriGridView();
        }

        private void ModificaTaskuriGridView()
        {
            taskuriGridView.Columns["idUser"].IsVisible = false;
            taskuriGridView.Columns["idTask"].IsVisible = false;
            taskuriGridView.Columns["TimpLucrat"].BestFit();
            taskuriGridView.Columns["Data"].BestFit();
            taskuriGridView.Columns["Data"].ReadOnly = true;
            taskuriGridView.Columns["TimpLucrat"].ReadOnly = true;
            taskuriGridView.Columns["Status"].ReadOnly = true;
        }

        private bool IsEmptyStudentiGridView()
        {
            return studentiGridView.Rows.Count < 1 ? true : false;
        }

        private void studentiGridView_CurrentRowChanged(object sender, Telerik.WinControls.UI.CurrentRowChangedEventArgs e)
        {
            try
            {
                AfiseazaTaskuriStudent();
            }
            catch
            {
                return;
            }
        }

        private void taskuriGridView_CellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (ValideazaTaskuriGridViewPentruEditare())
            {
                IncarcaEntitateaTaskDinGridView();
                int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
                TaskServices.UpdateTask(task, idTask);
            }
        }

        private bool ValideazaTaskuriGridViewPentruEditare()
        {
            if (taskuriGridView.CurrentRow.Cells["Denumire"].Value.ToString() == string.Empty || taskuriGridView.CurrentRow.Cells["Descriere"].Value.ToString() == string.Empty)
            {
                MessageBox.Show("Eroare!, Lipsa valoare.");
                AfiseazaTaskuriStudent();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void IncarcaEntitateaTaskDinGridView()
        {
            task.Denumire = taskuriGridView.CurrentRow.Cells["Denumire"].Value.ToString();
            task.Descriere = taskuriGridView.CurrentRow.Cells["Descriere"].Value.ToString();
        }

        private void taskuriGridView_UserDeletingRow(object sender, Telerik.WinControls.UI.GridViewRowCancelEventArgs e)
        {
            int idTask = (int)taskuriGridView.CurrentRow.Cells["idTask"].Value;
            TaskServices.DeleteTask(idTask);
        }

        private void adaugaTaskMenuItem_Click(object sender, EventArgs e)
        {
            adaugaTaskWindow.Show();
        }

        private void cancelTaskButton_Click(object sender, EventArgs e)
        {
            GolesteFormaDeAdaugareTask();
        }

        private void GolesteFormaDeAdaugareTask()
        {
            denumireTaskTextBox.Text = string.Empty;
            descriereTaskTextBox.Text = string.Empty;
        }

        private void adaugaTaskButton_Click(object sender, EventArgs e)
        {
            int idUser;
            try
            {
                idUser = Convert.ToInt32(studentiGridView.CurrentRow.Cells["idUser"].Value);
                AdaugaTaskStudent(idUser);
                GolesteFormaDeAdaugareTask();
            }
            catch (Exception)
            {
                MessageBox.Show("Selecteaza sau adauga un student!");
                GolesteFormaDeAdaugareTask();
                return;
            }
            AfiseazaTaskuriStudent();
        }

        private void AdaugaTaskStudent(int idUser)
        {
            if (!IsEmptyStudentiGridView())
            {
                if (ValideazaFormaAdaugareTask())
                {
                    IncarcaEntitateaTaskDinForma();
                    TaskServices.InsertTask(task, idUser);
                }
                else
                {
                    MessageBox.Show("Toate campurile sunt obligatorii!");
                }
            }
            else
            {
                MessageBox.Show("Selecteaza sau adauga un student!");
            }
        }

        private bool ValideazaFormaAdaugareTask()
        {
            if (string.IsNullOrWhiteSpace(denumireTaskTextBox.Text) || string.IsNullOrWhiteSpace(descriereTaskTextBox.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void IncarcaEntitateaTaskDinForma()
        {
            task.Denumire = denumireTaskTextBox.Text;
            task.Descriere = descriereTaskTextBox.Text;
            task.Status = 1;
            task.TimpLucrat = "00:00:00";
            task.DataCreare = GestioneazaBD.GetTimeStamp(DateTime.Now);
        }

        private bool ValideazaFormaAdaugareStudent()
        {
            try
            {
                double.IsNaN(Convert.ToDouble(notaStudentDropDown.SelectedValue));
                double.IsNaN(Convert.ToDouble(anStudentTextBox.Text));
                if (string.IsNullOrWhiteSpace(numeStudentTextBox.Text) || string.IsNullOrWhiteSpace(cnpStudentTextBox.Text) || string.IsNullOrWhiteSpace(adresaStudentTextBox.Text) || string.IsNullOrWhiteSpace(institutieStudentTextBox.Text))
                {
                    MessageBox.Show("Toate campurile sunt obligatorii!");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Campul 'An' trebuie sa fie de tip numeric!");
                return false;
            }
        }

        private void noteMenuItem_Click(object sender, EventArgs e)
        {
            NoteForm noteForma = new NoteForm();
            noteForma.Show();
        }

    }
}
